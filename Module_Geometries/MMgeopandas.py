# Copyright (c) 2020 Sophie Giffard-Roisin <sophie.giffard@univ-grenoble-alpes.fr>
# SPDX-License-Identifier: GPL-3.0

## Module to read and process shapefiles

import pandas as pd
import geopandas
from shapely.geometry import Point, Polygon, LineString, MultiPoint, MultiPolygon
from shapely.ops import nearest_points
from shapely.errors import TopologicalError
from copy import deepcopy

pd.options.mode.chained_assignment = None
from shapely.ops import polygonize

import numpy as np


def open_shpfile(name_file):
    '''
    :param name_file: name of the shape file to open
    :return: geopandas dataframe
    '''
    data = geopandas.read_file(name_file)
    return data


def save_shpfile(geodataframe, name_file):
    '''
    :param geodataframe: geopandas.geodataframe.GeoDataFrame
    :param name_file: .shp file (with path)
    :return:
    '''
    geodataframe.to_file(name_file, driver='ESRI Shapefile')


def get_subset_objects_ext_corrs(dataframe_corr):
    '''
    create a geopandas dataframe with only the external objects of corridors (insite_code=1)
    :param dataframe_corr: geopandas.geodataframe.GeoDataFrame
    :return: geopandas.geodataframe.GeoDataFrame
    '''

    dataframe_corr['is_external'] = [int(string[-1]) for string in dataframe_corr['INSITE_COD']]
    external = dataframe_corr[dataframe_corr['is_external'] == 1]
    return external


def get_subset_objects_within(geodataframe, polygon):
    '''
    get the subset of the geodataframe that is within a polygon
    :param geodataframe: geopandas.geodataframe.GeoDataFrame (ex: external corridors)
    :param polygon: Polygon
    :return:
    '''
    external_within = geodataframe[geodataframe.within(polygon)]
    return external_within


def get_subset_objects_interesects(geodataframe, polygon):
    '''
    get the subset of the geodataframe that is within a polygon
    :param geodataframe: geopandas.geodataframe.GeoDataFrame (ex: external corridors)
    :param polygon: Polygon
    :return:
    '''
    subset_interesect = geodataframe[geodataframe.intersects(polygon)]
    return subset_interesect


def get_corridor_ids_geodataframe(geodataframe):
    '''

    :param geodataframe: having the corridor objects
    :return: list of ids
    '''
    corr_ids = geodataframe['INSEE_SITE'].values
    corr_ids = list(map(int, corr_ids))
    return corr_ids


def save_corridor_ids(corr_ids, name_file_csv):
    '''
    :param corr_ids: list of ids
    :param name_file_csv: the file name with path to save the csv
    :return: 0
    '''
    # save the ids that will be the input of the 'sparse label matrix'
    np.savetxt(name_file_csv, corr_ids, delimiter=",")
    return 0


def clean_polygon(poly):
    if not poly.is_valid:
        seen = set()
        seen_add = seen.add
        vertices_noduplicates = [x for x in poly.exterior.coords if not (x in seen or seen_add(x))]
        poly = Polygon(vertices_noduplicates)
    if not poly.is_valid:
        poly = poly.simplify(tolerance=0.0001, preserve_topology=True)

    return poly


def clean_df_polygons(df_polygons):
    for i in range(len(df_polygons)):
        poly = df_polygons.geometry[i]
        if poly.is_valid:
            continue
        else:
            seen = set()
            seen_add = seen.add
            vertices_noduplicates = [x for x in poly.exterior.coords if not (x in seen or seen_add(x))]
            poly = Polygon(vertices_noduplicates)

            if not poly.is_valid:
                poly = poly.simplify(tolerance=0.0001, preserve_topology=True)

            df_polygons.geometry[i] = poly

    return df_polygons


def clean_df_linestrings(df_linestr):
    for i in range(len(df_linestr)):
        linestr = df_linestr.geometry[i]

        if linestr.geom_type == 'MultiLineString':  # to tackle at one point!
            linestr = list(linestr)[0]

        if len(set(linestr.coords)) < len(linestr.coords):
            seen = set()
            seen_add = seen.add
            vertices_noduplicates = [x for x in linestr.coords if not (x in seen or seen_add(x))]
            df_linestr.geometry[i] = LineString(vertices_noduplicates)

    return df_linestr


def get_insees_with_bottom(external_or_bottom):
    '''

    :param external_or_bottom:
    :return: get the insee which have both external and bottom in their corridor
    '''

    temp = external_or_bottom.groupby(["INSEE_SITE"])['is_external_or_bottom'].nunique().reset_index(name="count")
    insees_with_bottom = temp.loc[temp['count'] == 2, 'INSEE_SITE'].values.tolist()
    return insees_with_bottom


def get_df_polygons_from_df_lines(df_with_lines):
    '''
    get a dataframe with polygon geometries from a dataframe with line (or MultiLineString) objects.
    :param df_with_lines: geopandas dataframe with line objects
    :return: geopandas dataframe with polygon objects
    '''

    df_with_lines.index = range(len(df_with_lines))
    data_pandas = pd.DataFrame(df_with_lines.drop(columns='geometry'))
    polygons = []
    for i in range(len(df_with_lines)):
        list_points = []
        line = df_with_lines.geometry[i]
        if line.geom_type == "MultiLineString":
            for sub_line in line:
                list_points.extend(list(sub_line.coords))
        else:
            list_points = list(line.coords)
        poly = Polygon(list_points)
        polygons.append(poly)

    data_pandas['polygons'] = polygons
    df_polygons = geopandas.GeoDataFrame(data_pandas, geometry='polygons')
    df_polygons = clean_df_polygons(df_polygons)
    return df_polygons


def get_the_correct_extended_corridor_shape(data_pandas_external, coord_for_bottom):
    external_corridor = deepcopy(data_pandas_external)
    mid_pt = int(len(coord_for_bottom) / 2)
    data_pandas_external = coord_for_bottom[mid_pt:] + data_pandas_external + coord_for_bottom[:mid_pt]
    area1 = Polygon(data_pandas_external).area

    coord_for_bottom_rev = coord_for_bottom[::-1]
    external_corridor = coord_for_bottom_rev[mid_pt:] + external_corridor + coord_for_bottom_rev[:mid_pt]
    area2 = Polygon(external_corridor).area
    if area1 > area2 and Polygon(data_pandas_external).is_valid:
        return data_pandas_external
    else:
        return external_corridor


def bottom_intersecting_external(df_with_lines, insee_site):
    '''
    Checks if the external line of the corridor intersects the bottom line.
    :param df_with_lines:
    :param insee_site:
    :return: true if the bottom line intersects with the external line string
    '''
    ext_line = df_with_lines.geometry[(df_with_lines.INSEE_SITE == insee_site)
                                      & (df_with_lines.is_external_or_bottom == 1)]
    ext_line = ext_line[ext_line.index[0]]
    bottom_line = df_with_lines.geometry[(df_with_lines.INSEE_SITE == insee_site)
                                         & (df_with_lines.is_external_or_bottom == 4)]
    bottom_line = bottom_line[bottom_line.index[0]]
    if ext_line.intersects(bottom_line):
        return True
    else:
        return False
