# Copyright (c) 2020 Sophie Giffard-Roisin <sophie.giffard@univ-grenoble-alpes.fr>
# SPDX-License-Identifier: GPL-3.0

from geopandas import *
from shapely.ops import cascaded_union
from Module_Geometries.MMgeopandas import *


def get_bbox_polygon_from_tif(rasterio_img):
    '''

    :param rasterio_img:
    :return: Polygon
    '''
    bounding_box_img = rasterio_img.bounds
    Xminmax = [bounding_box_img.left, bounding_box_img.right]
    Yminmax = [bounding_box_img.bottom, bounding_box_img.top]

    bbox = Polygon([(Xminmax[0], Yminmax[0]), (Xminmax[0], Yminmax[1]),
                    (Xminmax[1], Yminmax[1]), (Xminmax[1], Yminmax[0])])
    return bbox


def get_polygon_from_rasterio_mask(rasterio_img):
    '''
    !!! Attention !!! not optimized code, does take time to run.
    :param rasterio_img: image from which you want to get a mask of non-zero values
    :return: polygon of the mask
    '''

    values = rasterio_img.read(1)
    mask = values != 0
    boxes = []
    for i in range(rasterio_img.height-1):
        for j in range(rasterio_img.width-1):
            if mask[i, j]:
                bbox = Polygon([rasterio_img.xy(i, j), rasterio_img.xy(i, j+1),
                                rasterio_img.xy(i+1, j+1), rasterio_img.xy(i+1, j)])
                boxes.append(bbox)

    polygon_mask = geopandas.GeoSeries(cascaded_union(boxes))
    return polygon_mask

