# Python tools for shapefiles and tiff images - compatible with Qgis

## Codes developed during the IRMAA project, learning avalanche deposits from satellite SAR images.
Authors: Sophie Giffard-Roisin, Saumya Sinha, Fatima Karbou

## Requirements (python 3):
* geopandas
* shapely
* rasterio
* numpy


## Installing requirements and cloning repository
sudo pip3 install geopandas shapely rasterio numpy

sudo apt-get install git-core

  cd MyFolderContainingMyCodes/

  git clone git@gricad-gitlab.univ-grenoble-alpes.fr:giffards/python_tools_shapefile_tiff.git

## Contents
The functions are in ModulesProcessing/

Some examples of how to use them with data examples are also here.

Mostly using polygons and lines.

## Please contribute if you have other functions