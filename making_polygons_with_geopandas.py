# Copyright (c) 2020 Sophie Giffard-Roisin <sophie.giffard@univ-grenoble-alpes.fr>
# SPDX-License-Identifier: GPL-3.0

## creating a geopandas dataframe with values from the shapefile and lines converted to polygons
## spoiler - if you just call Polygon on the line string coordinates, it converts into a polygon
import pandas as pd
from Module_Geometries import MMgeopandas
from shapely.geometry import MultiLineString,Point, Polygon, LineString

from copy import deepcopy


pd.options.display.max_rows = 10

shpfile_corridors_avalanches = 'Data_examples/EPA-73-74/EPA_Savoie_HteSavoie.shp'

print('Example: create polygons from partial lines')

print('1) create a dataframe of the objects contained in the shapefile')
df_raw = MMgeopandas.open_shpfile(shpfile_corridors_avalanches)
print('2) example of processing, recovering a subset of the shapefile object (external lines)...')
df_external = MMgeopandas.get_subset_objects_ext_corrs(df_raw)

print('3) example of creation of polygons from lines (simple version)')
df_polygons = MMgeopandas.get_df_polygons_from_df_lines(df_external)


print('4) writing shapefile with polygons')
MMgeopandas.save_shpfile(df_polygons, 'Data_examples/output_polygons.shp')














