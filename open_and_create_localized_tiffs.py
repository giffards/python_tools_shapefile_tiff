# Copyright (c) 2020 Sophie Giffard-Roisin <sophie.giffard@univ-grenoble-alpes.fr>
# SPDX-License-Identifier: GPL-3.0

# test to save a matrix as a rasterio (use the transform from another tiff image)

import pickle
import numpy as np
from Module_Geometries import MMrasterio

tif_file_template = 'Data_examples/SAR_example_HauteMaurienne.tif'

print('Example: same matrix as a tif image geolocalized')
# open existing image (template)
tif_rasterio = MMrasterio.open_tif(tif_file_template)

# create a matrix with numpy
Mat = np.zeros([tif_rasterio.height, tif_rasterio.width])

# (or open for ex. a .pkl file that you have)
#   with open(matrix_file, 'rb') as f:
#       Mat = pickle.load(f)
# transform it to have size height x width
# print(Mat.shape)
# Mat = Mat.T

transform = tif_rasterio.meta['transform']
file_saving = 'Data_examples/output.tif'

MMrasterio.save_array_to_tiff(Mat, transform, file_saving)
